import cv2

videoCapture = cv2.VideoCapture(0)
cv2.namedWindow("Video",cv2.WINDOW_NORMAL)
fps = 30
size = (int(videoCapture.get(cv2.CAP_PROP_FRAME_WIDTH)),int(videoCapture.get(cv2.CAP_PROP_FRAME_HEIGHT)))
videoWriter = cv2.VideoWriter("MyOutputVid.avi",cv2.VideoWriter_fourcc(*'I420'),fps,size)
numFrameRem = fps*10 - 1

'''
while success and numFrameRem > 0:
	n_frame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	videoWriter.write(n_frame)
	success, frame = videoCapture.read()
	numFrameRem -= 1
	'''
while (videoCapture.isOpened()):
	success, frame = videoCapture.read()
	numFrameRem -= 1
	if success == True and numFrameRem > 0:
		frm = cv2.flip(frame,1)
		cv2.imshow('Video',frm)
		videoWriter.write(frm)
		if cv2.waitKey(int(60/fps)) & 0xFF == ord('q'):
			break
	else:
		break
videoCapture.release()
videoWriter.release()
cv2.destroyAllWindows()