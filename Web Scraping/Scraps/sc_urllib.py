import urllib

fhand = urllib.urlopen('http://www.pythonlearn.com/html-008/cfbook013.html')
counts = dict()
for line in fhand:
	line = line.strip()
	words = line.split()
	for x in words:
		counts[x] = counts.get(x,0)+1
for key, value in sorted(counts.iteritems(), key=lambda (k,v): (v,k)):
    print "%s: %s" % (key, value)
