def capitalize(string):
    '''l = string.split()
    for i in range(len(l)):
    	if l[i][0].isalpha(): l[i] = l[i][0].upper() + l[i][1:]
    return ' '.join(l)'''
    l = list(string)
    l[0] = l[0].upper()
    for i in range(1,len(l)):
    	if l[i-1] == ' ':
    		l[i] = l[i].upper()
    return ''.join(l)


if __name__ == '__main__':
    string = raw_input()
    capitalized_string = capitalize(string)
    print capitalized_string