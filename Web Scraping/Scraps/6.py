N, M = map(int,raw_input().split()) # More than 6 lines of code will result in 0 score. Blank lines are not counted.
for i,j in zip(xrange(1,N,2),xrange(1,4)): 
    print ('.|.'*(i-j)).rjust((M-3)/2,'-') + '.|.' + ('.|.'*(i-j)).ljust((M-3)/2,'-')
print 'WELCOME'.center(M,'-')
for i,j in zip(xrange(N-2,-1,-2),xrange(3,0,-1)): 
    print ('.|.'*(i-j)).rjust((M-3)/2,'-') + '.|.' + ('.|.'*(i-j)).ljust((M-3)/2,'-')