def print_formatted(number):
    # your code goes here
    for i in range(1,number+1):
        l = len(bin(number)[2:])
        print str(i).rjust(l),oct(i)[1:].rjust(l),hex(i)[2:].rjust(l).upper(),bin(i)[2:].rjust(l)


if __name__ == '__main__':
    n = int(raw_input())
    print_formatted(n)