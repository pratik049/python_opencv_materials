import re

if __name__ == '__main__':
	file = open('test_file.txt')
	num = list()
	for line in file:
		line = line.rstrip()
		n = re.findall('[0-9]+',line)
		if len(n) < 1: continue
		for i in n:
			num.append(int(i))

print sum(num)