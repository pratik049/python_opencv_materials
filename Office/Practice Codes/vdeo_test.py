import cv2

videoCapture = cv2.VideoCapture('test_video.avi')
fps = videoCapture.get(cv2.CAP_PROP_FPS)
size = (int(videoCapture.get(cv2.CAP_PROP_FRAME_WIDTH)),int(videoCapture.get(cv2.CAP_PROP_FRAME_HEIGHT)))
videoWriter = cv2.VideoWriter('test_out.avi',cv2.VideoWriter_fourcc('P','I','M','1'),fps,size)

success, frame = videoCapture.read()
while success: # Loop until there are no more frames
	videoWriter.write(frame)
	success, frame = videoCapture.read()