import cv2
import numpy 
import os

# Make an array of 120,000 random bytes.
randomByteArray = bytearray(os.urandom(3147264))
flatNumpyArray = numpy.array(randomByteArray)

'''
The method commented below is more efficient

grayImage = numpy.random.randint(0,256,3147264).reshape(2304.1366)
'''

# Convert the array to make a 400x300 grayscale image.
grayImage = flatNumpyArray.reshape(2304,1366)
cv2.imwrite('RandomGray.png',grayImage)

# Convert the array to make a 400x100 color image.
bgrImage = flatNumpyArray.reshape(768,1366,3)
cv2.imwrite('RandomColor.png',bgrImage)