import cv2

clicked = False
def onMouse(event, x, y, flags, param):
    global clicked
    if event == cv2.EVENT_LBUTTONUP:
        clicked = True

cameraCapture = cv2.VideoCapture(0)
cv2.namedWindow('MyWindow',cv2.WINDOW_NORMAL)
cv2.setMouseCallback('MyWindow', onMouse)
# cv2.waitKey(1) & 0xFF == -1 and
print 'Showing camera feed. Click window or press any key to stop.'
success, frame = cameraCapture.read()
while success and not clicked:
    cv2.imshow('MyWindow', frame)
    success, frame = cameraCapture.read()

cv2.destroyWindow('MyWindow')
cameraCapture.release()